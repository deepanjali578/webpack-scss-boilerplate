* Store images that will be referenced by css.
* Webpack will auto copy them to `dist/images` folder.
* Any un-referenced file will be left here and will not be available.
