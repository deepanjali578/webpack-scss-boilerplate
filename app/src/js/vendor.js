// jQuery will be loaded from CDN, next line is here to satisfy bootstrap.js

import jQuery from "jquery";
// Exposing jQuery to window object
window.$ = window.jQuery = jQuery;

// import Popper from 'expose-loader?Popper!popper.js';
//Even if we have used expose-loader, we still need to expose Popper to window object
// window.Popper = Popper;

// Selective bootstrap.js build
import "bootstrap/js/dist/util";
import 'bootstrap/js/dist/dropdown';
import "bootstrap/js/dist/collapse";
import "bootstrap/js/dist/tab";
import "bootstrap/js/dist/modal";
import 'bootstrap/js/dist/carousel';

// to use aos animation
import AOS from "aos";
AOS.init();
// Polyfill
require("intersection-observer");

//for lazy loading images
import lozad from "lozad";
const observer = lozad(".lozad", {
  rootMargin: "200px 0px", // syntax similar to that of CSS Margin
  threshold: 0.1 // ratio of element convergence
});
observer.observe();

// We will extract all css to a separate file
require("../sass/vendor.scss");
require("../sass/app.scss");