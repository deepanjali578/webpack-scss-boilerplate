jQuery(document).ready(function($) {
  if ($(window).width() >= 1200) {
    // menu item highlight
    $("#siteNavbar").each(function() {
      var $nav = $(this);
      $nav
        .find(".navbar-nav")
        .first()
        .append('<li class="hover-highlighter"></li>')
        .find("li.nav-item")
        .hover(
          function() {
            $(this)
              .siblings(".hover-highlighter")
              .addClass("active")
              .css({
                // top: $(this).offset().top,
                left: $(this).offset().left,
                height: $(this).outerHeight()
              });
          },
          function() {
            $(this)
              .siblings(".hover-highlighter")
              .removeClass("active");
          }
        );

      // dropdownitem highlight
      $nav.find(".dropdown-menu").each(function() {
        $list = $(this);
        $list
          .find(".dropdown-item")
          .append('<span class="hover-highlighter"></span>')
          .find("a")
          .hover(
            function() {
              $(this)
                .siblings(".hover-highlighter")
                .addClass("active")
                .css({
                  height: $(this).outerHeight()
                });
            },
            function() {
              $(this)
                .siblings(".hover-highlighter")
                .removeClass("active");
            }
          );
      });

      // inner menu lightlight
      $nav.find(".inner-submenu").each(function() {
        $innerlist = $(this);
        $innerlist
          .find("li")
          .append('<span class="hover-highlighter"></span>')
          .find("a")
          .hover(
            function() {
              $(this)
                .siblings(".hover-highlighter")
                .addClass("active")
                .css({
                  height: $(this).outerHeight()
                });
            },
            function() {
              $(this)
                .siblings(".hover-highlighter")
                .removeClass("active");
            }
          );
      });
    });
  }

  // adding dropdown in mobile menu

  $(".dropdown .nav-link").after("<span></span>");
  $(".dropdown > span").addClass("dropdown-toggle");
  $(".dropdown .dropdown-toggle").click(function() {
    $(".dropdown").removeClass("expand");
    $(this)
      .parent(".dropdown")
      .addClass("expand");
    $(".expand .dropdown-menu").slideToggle("slow");
    $(this).toggleClass("toggled");
  });

  // modal navigation

  $(".team-member").click(function() {
    $("#teamModal .carousel-inner .carousel-item").removeClass("active");
    $("#teamModal .carousel-inner .carousel-item")
      .eq($(this).index())
      .addClass("active");
  });

  // navbar close btn
  $(".navbar-toggler").click(function() {
    $(".navbar-toggler").toggleClass("close-nav");
  });

  $(".site-header .btn-white.d-sm-none").click(function () {
    $(".navbar-collapse").removeClass("show");
    $(".navbar-toggler").removeClass("close-nav");
  });
  // tabs pagination
  // next
  $(".success-story-tab .next").click(function(e) {
    e.preventDefault();
    var nextText = $(".success-story-tab .nav-pills > .active")
      .next("a")
      .html();
    $(".success-story-tab .nav-pills > .active")
      .next("a")
      .trigger("click");
    $(".current-tab").text(nextText);
  });
  // prev
  $(".success-story-tab .prev").click(function(e) {
    e.preventDefault();
    var prevText = $(".success-story-tab .nav-pills > .active")
      .prev("a")
      .html();
    $(".success-story-tab .nav-pills > .active")
      .prev("a")
      .trigger("click");
    $(".current-tab").text(prevText);
  });
  // current tab name
  $(".success-story-tab .nav-link").click(function(e) {
    var currText = $(this).html();
    $(".current-tab").text(currText);
  });

  // hero-section
  $(".hero-section").each(function() {
    var $bgImages = $(this).find(".bg-images-container");
    var $marqueeImages = $(this).find(".marquee-images");
    // bg-images
    $bgImages.each(function() {
      setInterval(function() {
        var $activeImage = $bgImages.find(".active"),
          $nextImage = $activeImage.next().length
            ? $activeImage.next()
            : $bgImages.find(".bg-image").first();

        $activeImage.removeClass("active");
        $nextImage.addClass("active");
      }, 8000);
    });
    // marquee
    $marqueeImages.each(function() {
      var $imagesWrap = $(this)
          .find(".images-wrap")
          .first(),
        $imagesWrapImages = $imagesWrap.find("img");

      $marqueeImages.width(
        $imagesWrapImages.first().outerWidth(true) * $imagesWrapImages.length -
          210
      );
    });
  });

  // stop video on modal close
  $(".video-popup").on("hidden.bs.modal", function(e) {
    var $if = $(e.delegateTarget).find("iframe");
    var src = $if.attr("src");
    $if.attr("src", "/empty.html");
    $if.attr("src", src);
  });

  $(".video-popup").on("shown.bs.modal", function() {
    $(this)
      .find("video")[0]
      .play();
  });

  $(".video-popup").on("hidden.bs.modal", function() {
    $(this)
      .find("video")[0]
      .pause();
  });
});

$(".blog-tile video").each(function() {
  $(this).get(0).controls = false;
});

$(".blog-tile").each(function() {
  $(".play").click(function() {
    var video = $(this)
      .closest(".position-relative")
      .find("video")
      .get(0);
    video.play();
    video.controls = true;
    $(this).css("visibility", "hidden");
    return false;
  });
});

// remove unwanted classes on page load
$('#siteNavbar li.border-left').each(function (index, item) {
    if (!$(item).find('a').length) {
        $(item).removeClass("border-left")
    }
});
