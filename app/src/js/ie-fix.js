// IE Fixes

var userAgent, ieReg, ie;
userAgent = window.navigator.userAgent;
ieReg = /msie|Trident.*rv[ :]*11\./gi;
ie = ieReg.test(userAgent);

if (ie) {

  // fix for Background-attachment fixed
  $('.parallax-bg').css('background-attachment', 'inherit');

  // fix for Object-fit
  $(".img-container").each(function () {
    var $container = $(this),
      imgUrl = $container.find("img").prop("src");
    if (imgUrl) {
      $container.css("backgroundImage", 'url(' + imgUrl + ')').addClass("custom-object-fit");
    }
  });
}