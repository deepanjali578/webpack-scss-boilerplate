# FrontEnd Theme

### Prerequisites
* [node-js](https://github.com/creationix/nvm) >=10.5
* [yarn](https://yarnpkg.com/en/) >=1.16.x

### Setup
* Clone this repository, switch to current branch.
* Install dependencies
```
yarn install
```
* Start development
```
yarn run watch
```
* Start browser-sync
```
yarn start
```

### Building
* **Production**
```
yarn run prod
```
* **Development**
```
yarn run dev
```
* Above commands will generate `app/dist` folder


