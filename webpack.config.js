'use strict';

const webpack = require('webpack');
const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const TerserPlugin = require('terser-webpack-plugin');

const isProduction = process.env.NODE_ENV === 'production';

module.exports = {
  context: __dirname,
  mode: 'development',
  resolve: {
    modules: [
      path.resolve(__dirname, 'node_modules'),
    ],
    extensions: ['.wasm', '.mjs', '.js', '.json']
  },
  entry: {
    app: path.join(__dirname, 'app', "src", "js", "index.js"),
  },
  output: {
    path: path.join(__dirname, 'app', 'dist'),
    publicPath: '',
    filename: "js/[name].js",
    globalObject: 'this',
  },
  externals: {
    jquery: 'jQuery',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        // https://github.com/webpack/webpack/issues/2031
        exclude: /node_modules\/(?!(yt-player)\/).*/,
      },
      {
        test: /\.s?[ac]ss$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              sourceMap: !isProduction,
              importLoaders: 1,
            }
          },
          isProduction ? {
            loader: 'postcss-loader',
            options: {
              sourceMap: false,
              ident: 'postcss',
              plugins: (loader) => [
                require('autoprefixer')(),
                require('cssnano')({
                  preset: ['default', {
                    discardComments: {
                      removeAll: true,
                    },
                  }]
                })
              ]
            }
          } : false,
          {
            loader: 'sass-loader',
            options: {
              sourceMap: !isProduction,
              minimize: isProduction,
              implementation: require('sass'),
            }
          },
        ].filter(Boolean)
      },
      {
        // only include svg that doesn't have font in the path or file name by using negative lookahead
        test: /(\.(webp|ico|png|jpe?g|gif)$|^((?!font).)*\.svg$)/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 15000, // bytes
              fallback: 'file-loader',
              // file-loader options
              name: '[name]-[hash:8].[ext]',
              outputPath: 'images/',
              publicPath: '../images'
            }
          }
        ],
      },
      {
        // include fonts svg files only
        test: /(\.(woff2?|ttf|eot|otf)$|font.*\.svg$)/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name]-[hash:8].[ext]',
              outputPath: 'fonts/',
              publicPath: '../fonts'
            }
          },
        ],
      }
    ]
  },

  optimization: {
    minimizer: isProduction ? [
      new TerserPlugin({
        sourceMap: false,
        terserOptions: {
          output: {
            beautify: false,
          },
          compress: {
            drop_debugger: true,
            drop_console: true
          }
        }
      }),
    ] : [],
  },

  plugins: [
    new CleanWebpackPlugin({
      cleanStaleWebpackAssets: false,
    }),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery',
    }),
    new MiniCssExtractPlugin({
      filename: 'css/[name].css',
    }),
  ].concat(isProduction ? [] : [
    // https://webpack.js.org/plugins/source-map-dev-tool-plugin/
    new webpack.SourceMapDevToolPlugin(),
  ]),
  devtool: isProduction ? false : '#cheap-module-eval-source-map',
  performance: {
    hints: false,
  },
  stats: {
    modules: false,
    children: false,
    entrypoints: false,
  },
  bail: isProduction
};

